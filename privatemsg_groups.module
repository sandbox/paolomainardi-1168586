<?php

/**
 * @file
 * Allows to send messages to all members of a group.
 */

/**
 * Implements hook_perm().
 */
function privatemsg_groups_perm() {
  $perms = array('write privatemsg to all groups', 'write privatemsg to own groups');
  return $perms;
}

function privatemsg_groups_theme() {
  return array(
    'privatemsg_groups_format' => array(
      'arguments' => array('group' => NULL, 'options' => array()),
    ),
  );
}

/**
 * Implements hook_privatemsg_recipient_types_info().
 */
function privatemsg_groups_privatemsg_recipient_type_info() {
  return array(
    'group' => array(
      'name' => t('Group'),
      'description' => t('Enter the name of a group to write a message to all users that are suscribed to.'),
      'load' => 'privatemsg_groups_load_multiple',
      'format' => 'privatemsg_groups_format',
      'autocomplete' => 'privatemsg_groups_autocomplete',
      'generate recipients' => 'privatemsg_groups_load_recipients',
      'count' => 'privatemsg_groups_count_recipients',
      'write callback' => 'privatemsg_groups_write_access',
      'view callback' => 'privatemsg_group_view_access',
    ),
  );
}


function privatemsg_groups_form_privatemsg_admin_settings_alter(&$form, &$form_state) {
  $form['groups'] = array(
    '#type' => 'fieldset',
    '#title' => t('Privatemsg groups'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 25,
  );

  $form['groups']['privatemsg_group_admin_grant'] = array(
    '#title' => t('Restrict access to group administrators'),
    '#type'  => 'checkbox',
    '#default_value' => variable_get('privatemsg_group_admin_grant', 1),
    '#description' => t("Enabling this setting will restrict message sending only to group administrator."),
    '#weight' => -4,
  );
}

/**
 * View permission check for group recipients.
 */
function privatemsg_group_view_access($nid) {

  if (user_access('write privatemsg to all groups')) {
    return TRUE;
  }

  $nid = (is_object($nid) ? $nid->recipient : $nid);

  // if access is restricted to group admins
  if (variable_get('privatemsg_group_admin_grant', FALSE)) {
    return og_is_group_admin($nid);
  }
  return og_is_group_member($nid);
}

/**
 * Write permission check for group recipients.
 */
function privatemsg_groups_write_access($recipient) {
  global $user;
  
  // Check if user has permission to write to all groups.
  if (user_access('write privatemsg to all groups')) {
    return TRUE;
  }

  // Check if user has permission to write to own groups.
  if (!user_access('write privatemsg to own groups')) {
    return FALSE;
  }

  if ($recipient) {
    if (empty($recipient->title)) {
      $recipient = reset(privatemsg_groups_load_multiple(array($recipient->recipient)));
    }
    return privatemsg_group_view_access($recipient->recipient);
  }

  // check if user has group subscriptions
  return (bool) (count(og_get_subscriptions($user->uid)));
}

/**
 * Load a number of groups based on their nid.
 */
function privatemsg_groups_load_multiple($nids) {
  $result = db_query("SELECT n.title, n.nid AS recipient FROM {node} n JOIN {og} ON n.nid = og.nid WHERE n.status = 1 AND n.nid IN (" . db_placeholders($nids) . ")", $nids);
  $groups = array();
  while ($group = db_fetch_object($result)) {
    $group->type = 'group';
    $groups[privatemsg_recipient_key($group)] = $group;
  }
  
  return $groups;
}

/**
 * Format a group to be displayed as a recipient.
 */
function theme_privatemsg_groups_format($group, $options = array()) {
  if (!empty($options['plain'])) {
    $name = $group->title;
    if (!empty($options['unique'])) {
      $name .= ' [group]';
    }
    return $name.'(group)';
  }
  return t('%group (group)', array('%group' => $group->title));
}

/**
 * Loads users with a specific group.
 */
function privatemsg_groups_load_recipients($recipient, $limit, $offset) {
  $recipients = array();
  $nid = isset($recipient->recipient) ? $recipient->recipient : $recipient->nid;
  $result = db_query(og_list_users_sql(1, 0, null, false), $nid);
  while ($row = db_fetch_object($result)) {
    $recipients[] = $row->uid;
  }
  return $recipients;
}

/**
 * Return the number of users which have a given group.
 */
function privatemsg_groups_count_recipients($recipient) {
  $nid = isset($recipient->recipient) ? $recipient->recipient : $recipient->nid;
  return db_result(db_query(og_list_users_sql(1, 0, null, true), $nid));
}

/**
 * Provides autocomplete suggestions for groups.
 */
function privatemsg_groups_autocomplete($fragment, $names, $limit) {
  $query = _privatemsg_assemble_query(array('autocomplete_groups', 'privatemsg_groups'), $fragment, $names);
  $result = db_query_range($query['query'], $fragment, 0, $limit);
  // 3: Build proper suggestions and print.
  $groups = array();
  while ($group = db_fetch_object($result)) {
    if (user_access('write privatemsg to all groups') || (privatemsg_group_view_access($group->nid))) {
      $group->type = 'group';
      $group->recipient = $group->nid;
      $groups[privatemsg_recipient_key($group)] = $group;
    }
  }
  return $groups;
}

/**
 * Implements hook_privatemsg_name_lookup().
 */
function privatemsg_groups_privatemsg_name_lookup($string) {
  // Remove optonal group specifier.
  $string = trim(str_replace(array(t('[group]'), '(group)'), '', $string));
  $result = db_query("SELECT n.*, n.nid AS recipient FROM {node} n JOIN {og} ON og.nid = n.nid WHERE n.title = '%s' AND n.status = 1", trim($string));
  if ($group = db_fetch_object($result)) {
    $group->type = 'group';
    return array(privatemsg_recipient_key($group) => $group);
  }
}

/**
 * Query definition to search for username autocomplete suggestions.
 *
 * @param $fragments
 *   Query fragments array.
 * @param $search
 *   Which search string is currently searched for.
 * @param $names
 *   Array of names not to be used as suggestions.
 */
function privatemsg_groups_sql_autocomplete_groups(&$fragments, $search, $names) {
  $fragments['primary_table'] = '{node} n';
  $fragments['select'][] = 'n.title, n.nid';

  // Escape the % to get it through the placeholder replacement.
  // Join another table.
  $fragments['inner_join'][] = 'JOIN {og} og ON (n.nid = og.nid)';
  $fragments['where'][] = "n.title LIKE '%s' AND n.status = 1";
  $fragments['query_args']['where'][] = $search .'%%';
  if (!empty($names)) {
    // If there are already names selected, exclude them from the suggestions.
    $fragments['where'][] = "n.title NOT IN (". db_placeholders($names, 'text') .")";
    $fragments['query_args']['where'] += $names;
  }
 
  $fragments['order_by'][] = 'n.title ASC';
}
